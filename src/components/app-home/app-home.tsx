import { Component, State, Watch } from '@stencil/core';

@Component({
  tag: 'app-home',
  styleUrl: 'app-home.scss',
  shadow: false
})
export class AppHome {
  private map: google.maps.Map;
  private infowindow: google.maps.InfoWindow;
  private initFlag:boolean=false;
  @State()
  private apiKey: string ;
  @State()
  private usrPos: google.maps.LatLngLiteral;
  @State()
  private restaurants: Array<any> = [];
  @State()
  private errMessage: string;
  @State()
  private rankBy: google.maps.places.RankBy;

  private markers: Array<google.maps.Marker> = [];
  componentDidLoad() {
  }
  componentDidUpdate(){
    if(this.initFlag){
      this.initFlag=false;
      this.init();
    }
  }
  init(){
    this.injectSDK()
    .then(() =>
      this.initMap()
    ).then(() => {
      this.findRestaurant()
    })
    .then(() => {
      this.map.addListener('bounds_changed', this.debounce(this.boundChangeHandler.bind(this), 500));
    }).catch((err: Error) => {
      this.errMessage = err.message;
    });

  }
  debounce(func, delay) {
    let timer = null;
    return function () {
      let context = this;
      let args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        func.apply(context, args)
      }, delay);
    }
  }
  boundChangeHandler() {
    if (this.map) {
      this.usrPos = {
        lat: this.map.getCenter().lat(),
        lng: this.map.getCenter().lng()
      }
      this.refresh();
    }
  }
  @Watch('rankBy')
  rankByChangeHandler() {
    this.refresh();
  }
  refresh() {
    this.findRestaurant().catch(err=>{this.errMessage=err.message});
  }
  findRestaurant(): Promise<any> {
    let request;
    switch (this.rankBy) {
      case google.maps.places.RankBy.DISTANCE:
        request = {
          location: this.usrPos,
          type: "restaurant",
          rankBy: this.rankBy
        };
        break;
      default:
        request = {
          location: this.usrPos,
          type: "restaurant",
          radius: 500,
          rankBy: this.rankBy
        };
    }


    let service = new google.maps.places.PlacesService(this.map);
    return new Promise((resolve, reject) => {
      service.nearbySearch(request, (results, err) => {

        if (results) {
          this.restaurants = results;
          resolve();
        }
        if(err)
        reject(err)
      });

    })


  }
  createMarker(place) {
    let marker = new google.maps.Marker({
      map: this.map,
      position: place.geometry.location
    });
    let ctrl = this;
    google.maps.event.addListener(marker, 'click', function () {
      //ctrl.infowindow.setContent(<div><img src={place.icon}></img>{place.name}</div>);
      ctrl.infowindow.open(ctrl.map, this);
    });
    this.markers.push(marker);
    return marker;
  }
  initMap(): Promise<any> {
    this.map = new google.maps.Map(document.getElementById('map'), { zoom: 17, center: { lat: 0, lng: 0 } });
    this.infowindow = new google.maps.InfoWindow();
    return new Promise((resolve, reject) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.usrPos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          this.map.setCenter(this.usrPos);

          resolve();
        }, () => {
          reject(new Error("Error: The Geolocation service failed."))
        });
      } else {
        reject(new Error("Error: Your browser doesn\'t support geolocation."))
      }
    })
  }
  injectSDK(): Promise<any> {
    (window as any).gm_authFailure = ()=> {
      this.errMessage="InvalidKeyMapError";
  }
    return new Promise((resolve) => {
      let script = document.createElement('script');
      script.id = 'googleMaps';
      script.async = true;
      script.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&libraries=places';
      script.onload = () => {
        resolve();
      }
      document.body.appendChild(script);
    }
    )

  }
  getRestPhoto(resturant: any) {
    return resturant.photos ? resturant.photos[0].getUrl() : "";
  }
  deleteMarkers() {
    this.markers.forEach(marker => {
      marker.setMap(null);
    })
    this.markers = [];
  }
  goToRest(resturant: any) {
    this.deleteMarkers();
    let marker = this.createMarker(resturant);
    this.map.panTo(resturant.geometry.location);
    this.infowindow.setContent(`<div class=" d-flex align-items-center">
    <img width="50" height="50" src="`+ this.getRestPhoto(resturant) + `" />
    <div class="p-2 text-center">`+ resturant.name + `</div>
    </div>
    <div class="mt-2">`+ resturant.vicinity + `</div>`)
    this.infowindow.open(this.map, marker);

  }
  formSubmitHandler(e:Event){
    e.preventDefault();
    let formData=new FormData(e.target as HTMLFormElement);
    this.apiKey=formData.get("apikey").toString();
    this.initFlag=true;
  }
  render() {
    if (this.apiKey&&!this.errMessage) {
      return (
        <div class='h-100 w-100 d-flex flex-column flex-md-row'>
          <div id="map" class="flex-md-grow-1">
          </div>
          <div id="info-panel" class="overflow-auto p-2">
            <h2 class="mb-3">Nearby Restaurant</h2>
            <div class="mb-2 btn-group w-100" role="group">
              <button type="button" class="btn btn-primary p-2" onClick={() => this.rankBy = google.maps.places.RankBy.PROMINENCE}>Rank By Prominence</button>
              <button type="button" class="btn btn-primary" onClick={() => this.rankBy = google.maps.places.RankBy.DISTANCE}>Distance</button>
            </div>
            {this.restaurants ? this.restaurants.map(resturant =>
              <div class="btn btn-light d-flex mb-2 rounded border p-2 flex-column text-left cursor-pointer" onClick={() => this.goToRest(resturant)}>
                <div class=" d-flex align-items-center">
                  <div class="rest-icon" style={{ backgroundImage: "url(" + this.getRestPhoto(resturant) + ")" }}></div>
                  <div class="p-2">{resturant.name}</div>

                </div>
                <div class="mt-2">{resturant.vicinity}</div>
              </div>

            ) : ""}

            {this.errMessage}</div>
        </div>
      );
    }
    else if(this.errMessage){
      return <div class="w-100 h-100 d-flex align-items-center justify-content-center p-4 flex-column text-center">
        {this.errMessage}
      </div>
    }
    else {
      return <div class="w-100 h-100 d-flex align-items-center justify-content-center p-4 flex-column text-center">
      <h4>Please enter your Google Map Platform API key :</h4>
      <form class="form-control" onSubmit={(e)=>this.formSubmitHandler(e)}>
        <input name="apikey" type="text" class="form-control border mt-4" required></input>
        <button type="submit" class="btn btn-primary mt-4">Okay</button>
        </form>
      </div>
    }
  }
}